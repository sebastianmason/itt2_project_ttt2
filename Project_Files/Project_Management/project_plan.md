---
title: 'ITT2_project_TTT'
subtitle: 'Project Plan'
authors: ['Thomas Bargisen thom8723@edu.eal.dk', 'Tihamer Biliboc tiha0006@edu.eal.dk', 'Sebastian Mason seba7286@edu.eal.dk']
date: \today
left-header: \today
right-header: Project Plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose

The main purpose of this project is to create a small system in which sensors are able to interact, collect and send data to a webserver through a pre-determined network layout.
Among features, we were left with some lee-way to choose in which way these/this sensor is able to be used to solve a problem.

Our proposed issue is in regards to greenhouses and adjusting temperature to get the best growth out of your plants.
Greenhouses are often regulated by opening and closing windows, letting in cold air and letting out hot air.
This causes the humidity to rise and fall, keeping the plants at their optimal temperature, which we can easily regulate using a Temperature Sensitive Sensor and a Window Opening Mechanism.

We plan on attaching this sensor to the inside of a greenhouse as well as the window opener to the window. A pre-determined humidity or temperature will then cause the window to open, or close.

Optionally, a button may be added to force close or open the window, in case of emergencies and testing.

Additionally, we plan on sending information such as timestamps of opening/closing and the humidity and temperature inside the greenhouse to a webserver through the pre-determined layout, resulting in a logging of actions so that the user can adjust to their liking.

---------------
The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated will evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](Project_Files/Project_Management/Placeholder_Diagram1.png)

Reading from the left to the right
* Analog and digital input: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature and a humidity sensor.
* ATtiny85: Microcontroller to run the sensor software and to be able to interface with Raspberry Pi using SPI.
* Raspberry Pi: Minimal Linux system relevant programs to host data from the ATtiny and to/from the user interface.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* Documentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and GitLab will be used as the project management platform.


# Schedule

The project is divided into three phases from start to the end of the semester.

See the lecture plan for details.


# Organization
## Group Members
- Sebastian Mason
- Tihamer Biliboc
- Thomas Bargisen

## Advisors / Overseers
- Nikolaj Simonsen
- Morten Nielsen

## External resources
### Knowledge
- Classmates
- Teachers


# Budget and resources

In terms of manpower, only the people in the project group are expected to contribute.
The total estimated material cost is 600 dkk: Cellular extension for data transfer - 500 dkk, Temperature and humidity sensor - 3 x 33,33 dkk



# Risk assessment

Risk assessment is a list of possible risks to the project, and suggestions on how to solve them.

## Project risks

- Teamwork
- Lack of documentation
- Poor project management
- Lack of knowledge
- Lack of motivation
- Electrical failures
- Sick leave

## How to overcome these issues

### Teamwork
- Meetings
- Peer pressure
- Talk to advisor
- Kick out


### Documentation

- Lack of teacher docs
- Lack of student docs
- Who does what in the project, take notes! so you can identify the issues.

### Project Management

- Make sure to keep group meetings, a regular thing. So all are on the same page!
- Never leave issues unresolved! make priorities

### Knowledge

- Seek knowledge / help from class mates.
- Group pressure
- Be honest with the goals, never hide your progress!
- Reduce scope of the tasks/project


### Motivation.

- If you lack the motivation, talk with the team. find a solution!

### Electrical Failures
- Make backups
- Spare parts
- Triple check everything
- Don't make high risk decisions

### Sick Leave
- Keep contact
- Redistribute workflow
- Try to work from home


# Stakeholders
The stakeholders include everyone that is actively involved in the project

## Group members
Actively working on tasks to complete the project
- Sebastian Mathias Thomle Mason
- Tihamer Biliboc
- Thomas Tøttrup Bargisen

## Project coordinators
Actively overseeing the project
- Nikolaj Simonsen
- Morten Nielsen

## Customers
Actively interested in the development, financing and use of the product
### Private
- Neighbors
- Small greenhouse owners
### Companies
- Agricultural industry


# Communication

## Group members
All communication within the group will take place via Discord, Messenger or other messaging applications


## Project coordinators
Communication to and from the project coordinators will manifest mainly via weekly meetings, itslearning and e-mails

## Customers
Communication to and from the customers will manifest mainly via e-mail, phone or in person
### Private
- Neighbors
- Small greenhouse owners
### Companies
- Agricultural industry


# Perspectives

This project will serve as a template for other similar project, like the  one in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

## All tests succeed
- read temperature
- send temperature value to server
- read air humidity
- send humidity value to server
- open / close window from button press
- open / close window from server
- open / close window based on temperature from sensor
- start / stop ventilation, open / close window based on humidity

## Customer satisfaction
- Feedback from customers after an organized product test

## All project milestones on GitLab are completed


# References

None at this time
