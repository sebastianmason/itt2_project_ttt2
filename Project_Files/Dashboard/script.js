var baseUrl = 'http://192.168.43.253:5000/'
$(document).ready(function(){

    httpGet(baseUrl+'fan').then((getResponse) => {
        updateElement(getResponse.toString() == "true" ? 1 : 0, baseUrl+'fan');
    }); 

    var ctx = document.getElementById("myChart").getContext('2d');
    window.iot_graphs = new Chart(ctx, config);
    
    setInterval(updateGraph, 10000);

    $("#fanBtn").click(async () => {
        let url = baseUrl + 'fan';
        let putResponse = await httpPut(url, {});
        if (putResponse.ok) {
            let getResponse = await httpGet(url);
            updateElement(getResponse.toString() == "true" ? 1 : 0, url);
        }
        else {
            console.error(putResponse)
        }     
    });

    $("#upBtn").click(async () => {
        let url = baseUrl+'roof';
        let postResponse = await httpPost(url, {"data":1});
        if (postResponse.ok) {
            let getResponse = await httpGet(url);
        }
        else {
            console.error(postResponse)
        }     
    });

    $("#downBtn").click(async () => {
        let url = baseUrl+'roof';
        let postResponse = await httpPost(url, {"data":0});
        if (postResponse.ok) {
            let getResponse = await httpGet(url);
        }
        else {
            console.error(postResponse)
        }     
    });

    $("#update-btn").click(async () => {        
        updateGraph()
        });
});

async function updateGraph(){
    let tempResponse = await httpGet(baseUrl+'temp');
    let humidResponse = await httpGet(baseUrl+'humid');
    addData(window.iot_graphs, curTime(), [tempResponse, humidResponse]);
    updateLastMessage(`${tempResponse.toString()} added to graph`)
    updateLastMessage(`${humidResponse.toString()} added to graph`)
}

//HTTP FUNCTIONS
async function httpGet(url){
    try {
        let response = await fetch(url);
        updateLastMessage(`${response.status} ${response.statusText}`);
        let json = await response.json();
        return json;
    } catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

async function httpPut(url, data) {
    try {
        let response = await fetch(url, {
            method: 'PUT', // 'GET', 'PUT', 'DELETE', etc.
            body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
            headers: new Headers({
                'Content-Type': 'application/json;charset=UTF-8'
            })
        });
        updateLastMessage(`${response.status} ${response.statusText}`);
        return response;
    }
    catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

async function httpPost(url, data) {
    try {
        let response = await fetch(url, {
            method: 'POST', // 'GET', 'PUT', 'DELETE', etc.
            body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
            headers: new Headers({
                'Content-Type': 'application/json;charset=UTF-8'
            })
        });
        updateLastMessage(`${response.status} ${response.statusText}`);
        return response;
    }
    catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

//OUTPUTS
function updateElement(data, url){
    let element = document.getElementById('status-field-'+url.substr(-3)); //element status-field-A0 etc. 
    //value = data.value;
    element.innerHTML = data;
    parseInt(data) ? element.style.backgroundColor = "darkolivegreen" : element.style.backgroundColor = "tomato";
}

function updateLastMessage(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text}`));
    $('#api-log').prepend(li);
}

function addData(chart, label, data) {
    //console.log(chart);
    let i = 0
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data[i]);
        i++;
        if (dataset.data.length > 10) {
            removeData(chart)
        };  
    });    
    chart.update();
}

function removeData(chart) {
    chart.data.labels.shift();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.shift();
    });
    chart.update();
}

//CHART CONFIG
var config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Temprature',
            yAxisID: 'temp',
            fill: false,
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: []					
            },
            {
            label: 'Humidity',
            yAxisID: 'humid',
            fill: false,
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []					
            }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: 'Temp/humid graph'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                id: 'temp',
                type: 'linear',
                position: 'left',
                scaleLabel: {
                    display: true,
                    labelString: 'Temprature °C'
                  }
              }, {
                id: 'humid',
                type: 'linear',
                position: 'right',
                scaleLabel: {
                    display: true,
                    labelString: 'Humidity %'
                  }
              }]
        }
    }
};

//GET TIME
function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}
