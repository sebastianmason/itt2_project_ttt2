<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="e14_Rpi3_RevA">
<packages>
<package name="RPI3">
<description>Package for the Raspberry Pi 3 board created by Dave Young for www.elemenet14.com based on documented dimensions and board measurements.
For more documentation, please visit http://www.raspberrypi.org/documentation/hardware/raspberrypi/</description>
<wire x1="0" y1="53" x2="0" y2="3" width="0.127" layer="51"/>
<wire x1="0" y1="3" x2="3" y2="0" width="0.127" layer="51" curve="90"/>
<wire x1="3" y1="0" x2="82" y2="0" width="0.127" layer="51"/>
<wire x1="82" y1="0" x2="85" y2="3" width="0.127" layer="51" curve="90"/>
<wire x1="85" y1="3" x2="85" y2="53" width="0.127" layer="51"/>
<wire x1="85" y1="53" x2="82" y2="56" width="0.127" layer="51" curve="90"/>
<wire x1="82" y1="56" x2="3" y2="56" width="0.127" layer="51"/>
<wire x1="3" y1="56" x2="0" y2="53" width="0.127" layer="51" curve="90"/>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="52.5" drill="2.75"/>
<hole x="61.5" y="52.5" drill="2.75"/>
<circle x="3.5" y="3.5" radius="3.04795" width="0.127" layer="51"/>
<circle x="61.5" y="3.5" radius="3" width="0.127" layer="51"/>
<circle x="61.5" y="52.5" radius="3" width="0.127" layer="51"/>
<circle x="3.5" y="52.5" radius="3" width="0.127" layer="51"/>
<wire x1="70" y1="22" x2="87" y2="22" width="0.127" layer="51"/>
<wire x1="87" y1="22" x2="87" y2="35" width="0.127" layer="51"/>
<wire x1="87" y1="35" x2="70" y2="35" width="0.127" layer="51"/>
<wire x1="70" y1="35" x2="70" y2="22" width="0.127" layer="51"/>
<wire x1="70" y1="41" x2="87" y2="41" width="0.127" layer="51"/>
<wire x1="87" y1="41" x2="87" y2="54" width="0.127" layer="51"/>
<wire x1="87" y1="54" x2="70" y2="54" width="0.127" layer="51"/>
<wire x1="70" y1="54" x2="70" y2="41" width="0.127" layer="51"/>
<wire x1="87" y1="18.3" x2="66" y2="18.3" width="0.127" layer="51"/>
<wire x1="66" y1="18.3" x2="66" y2="2.3" width="0.127" layer="51"/>
<wire x1="66" y1="2.3" x2="87" y2="2.3" width="0.127" layer="51"/>
<wire x1="87" y1="2.3" x2="87" y2="18.3" width="0.127" layer="51"/>
<wire x1="58.239" y1="55.0018" x2="6.739" y2="55.0018" width="0.127" layer="21"/>
<wire x1="6.739" y1="55.0018" x2="6.739" y2="51.0018" width="0.127" layer="21"/>
<wire x1="6.739" y1="51.0018" x2="7.739" y2="50.0018" width="0.127" layer="21"/>
<wire x1="7.739" y1="50.0018" x2="58.239" y2="50.0018" width="0.127" layer="21"/>
<wire x1="58.239" y1="50.0018" x2="58.239" y2="55.0018" width="0.127" layer="21"/>
<wire x1="57" y1="12.5" x2="50" y2="12.5" width="0.127" layer="51"/>
<wire x1="50" y1="12.5" x2="50" y2="0" width="0.127" layer="51"/>
<wire x1="50" y1="0" x2="50.5" y2="0" width="0.127" layer="51"/>
<wire x1="50.5" y1="0" x2="50.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="50.5" y1="-2.5" x2="56.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="56.5" y1="-2.5" x2="56.5" y2="0" width="0.127" layer="51"/>
<wire x1="56.5" y1="0" x2="57" y2="0" width="0.127" layer="51"/>
<wire x1="57" y1="0" x2="57" y2="12.5" width="0.127" layer="51"/>
<wire x1="5.6" y1="39" x2="5.6" y2="17" width="0.127" layer="51"/>
<wire x1="5.6" y1="17" x2="2.6" y2="17" width="0.127" layer="51"/>
<wire x1="2.6" y1="17" x2="2.6" y2="23" width="0.127" layer="51"/>
<wire x1="2.6" y1="23" x2="1.6" y2="24" width="0.127" layer="51"/>
<wire x1="1.6" y1="24" x2="1.6" y2="33" width="0.127" layer="51"/>
<wire x1="1.6" y1="33" x2="2.6" y2="34" width="0.127" layer="51"/>
<wire x1="2.6" y1="34" x2="2.6" y2="39" width="0.127" layer="51"/>
<wire x1="2.6" y1="39" x2="5.6" y2="39" width="0.127" layer="51"/>
<wire x1="44" y1="0.5" x2="44" y2="22.5" width="0.127" layer="51"/>
<wire x1="44" y1="22.5" x2="47" y2="22.5" width="0.127" layer="51"/>
<wire x1="47" y1="22.5" x2="47" y2="16.5" width="0.127" layer="51"/>
<wire x1="47" y1="16.5" x2="48" y2="15.5" width="0.127" layer="51"/>
<wire x1="48" y1="15.5" x2="48" y2="6.5" width="0.127" layer="51"/>
<wire x1="48" y1="6.5" x2="47" y2="5.5" width="0.127" layer="51"/>
<wire x1="47" y1="5.5" x2="47" y2="0.5" width="0.127" layer="51"/>
<wire x1="47" y1="0.5" x2="44" y2="0.5" width="0.127" layer="51"/>
<wire x1="39" y1="11" x2="24" y2="11" width="0.127" layer="51"/>
<wire x1="24" y1="11" x2="24" y2="-1" width="0.127" layer="51"/>
<wire x1="24" y1="-1" x2="39" y2="-1" width="0.127" layer="51"/>
<wire x1="39" y1="-1" x2="39" y2="11" width="0.127" layer="51"/>
<wire x1="14.6" y1="5" x2="6.6" y2="5" width="0.127" layer="51"/>
<wire x1="6.6" y1="5" x2="6.6" y2="-1" width="0.127" layer="51"/>
<wire x1="6.6" y1="-1" x2="14.6" y2="-1" width="0.127" layer="51"/>
<wire x1="14.6" y1="-1" x2="14.6" y2="5" width="0.127" layer="51"/>
<pad name="P$1" x="8.37" y="51.23" drill="1.016" shape="square"/>
<pad name="P$2" x="8.3566" y="53.7718" drill="1.016"/>
<pad name="P$3" x="10.8966" y="51.2318" drill="1.016"/>
<pad name="P$4" x="10.8966" y="53.7718" drill="1.016"/>
<pad name="P$5" x="13.4366" y="51.2318" drill="1.016"/>
<pad name="P$6" x="13.4366" y="53.7718" drill="1.016"/>
<pad name="P$7" x="15.9766" y="51.2318" drill="1.016"/>
<pad name="P$8" x="15.9766" y="53.7718" drill="1.016"/>
<pad name="P$9" x="18.5166" y="51.2318" drill="1.016"/>
<pad name="P$10" x="18.5166" y="53.7718" drill="1.016"/>
<pad name="P$11" x="21.0566" y="51.2318" drill="1.016"/>
<pad name="P$12" x="21.0566" y="53.7718" drill="1.016"/>
<pad name="P$13" x="23.5966" y="51.2318" drill="1.016"/>
<pad name="P$14" x="23.5966" y="53.7718" drill="1.016"/>
<pad name="P$15" x="26.1366" y="51.2318" drill="1.016"/>
<pad name="P$16" x="26.1366" y="53.7718" drill="1.016"/>
<pad name="P$17" x="28.6766" y="51.2318" drill="1.016"/>
<pad name="P$18" x="28.6766" y="53.7718" drill="1.016"/>
<pad name="P$19" x="31.2166" y="51.2318" drill="1.016"/>
<pad name="P$20" x="31.2166" y="53.7718" drill="1.016"/>
<pad name="P$21" x="33.7566" y="51.2318" drill="1.016"/>
<pad name="P$22" x="33.7566" y="53.7718" drill="1.016"/>
<pad name="P$23" x="36.2966" y="51.2318" drill="1.016"/>
<pad name="P$24" x="36.2966" y="53.7718" drill="1.016"/>
<pad name="P$25" x="38.8366" y="51.2318" drill="1.016"/>
<pad name="P$26" x="38.8366" y="53.7718" drill="1.016"/>
<pad name="P$27" x="41.3766" y="51.2318" drill="1.016"/>
<pad name="P$28" x="41.3766" y="53.7718" drill="1.016"/>
<pad name="P$29" x="43.9166" y="51.2318" drill="1.016"/>
<pad name="P$30" x="43.9166" y="53.7718" drill="1.016"/>
<pad name="P$31" x="46.4566" y="51.2318" drill="1.016"/>
<pad name="P$32" x="46.4566" y="53.7718" drill="1.016"/>
<pad name="P$33" x="48.9966" y="51.2318" drill="1.016"/>
<pad name="P$34" x="48.9966" y="53.7718" drill="1.016"/>
<pad name="P$35" x="51.5366" y="51.2318" drill="1.016"/>
<pad name="P$36" x="51.5366" y="53.7718" drill="1.016"/>
<pad name="P$37" x="54.0766" y="51.2318" drill="1.016"/>
<pad name="P$38" x="54.0766" y="53.7718" drill="1.016"/>
<pad name="P$39" x="56.6166" y="51.2318" drill="1.016"/>
<pad name="P$40" x="56.6166" y="53.7718" drill="1.016"/>
<text x="6" y="49" size="0.8128" layer="21">J8</text>
<wire x1="2.6" y1="34" x2="2.6" y2="39" width="0.127" layer="21"/>
<wire x1="2.6" y1="39" x2="5.6" y2="39" width="0.127" layer="21"/>
<wire x1="5.6" y1="39" x2="5.6" y2="34" width="0.127" layer="21"/>
<wire x1="2.6" y1="23" x2="2.6" y2="17" width="0.127" layer="21"/>
<wire x1="2.6" y1="17" x2="5.6" y2="17" width="0.127" layer="21"/>
<wire x1="5.6" y1="17" x2="5.6" y2="23" width="0.127" layer="21"/>
<wire x1="6.6" y1="0.1" x2="6.6" y2="5" width="0.127" layer="21"/>
<wire x1="6.6" y1="5" x2="14.6" y2="5" width="0.127" layer="21"/>
<wire x1="14.6" y1="5" x2="14.6" y2="0.1" width="0.127" layer="21"/>
<wire x1="24" y1="0.1" x2="24" y2="11" width="0.127" layer="21"/>
<wire x1="24" y1="11" x2="39" y2="11" width="0.127" layer="21"/>
<wire x1="39" y1="11" x2="39" y2="0.1" width="0.127" layer="21"/>
<wire x1="47" y1="5.5" x2="47" y2="0.5" width="0.127" layer="21"/>
<wire x1="47" y1="0.5" x2="44" y2="0.5" width="0.127" layer="21"/>
<wire x1="44" y1="0.5" x2="44" y2="5.5" width="0.127" layer="21"/>
<wire x1="47" y1="16.5" x2="47" y2="22.5" width="0.127" layer="21"/>
<wire x1="47" y1="22.5" x2="44" y2="22.5" width="0.127" layer="21"/>
<wire x1="44" y1="22.5" x2="44" y2="16.5" width="0.127" layer="21"/>
<wire x1="50" y1="0.1" x2="50" y2="12.5" width="0.127" layer="21"/>
<wire x1="50" y1="12.5" x2="57" y2="12.5" width="0.127" layer="21"/>
<wire x1="57" y1="12.5" x2="57" y2="0.1" width="0.127" layer="21"/>
<wire x1="84.7" y1="2.3" x2="66" y2="2.3" width="0.127" layer="21"/>
<wire x1="66" y1="2.3" x2="66" y2="18.3" width="0.127" layer="21"/>
<wire x1="66" y1="18.3" x2="84.8" y2="18.3" width="0.127" layer="21"/>
<wire x1="84.8" y1="22" x2="70" y2="22" width="0.127" layer="21"/>
<wire x1="70" y1="22" x2="70" y2="35" width="0.127" layer="21"/>
<wire x1="70" y1="35" x2="84.8" y2="35" width="0.127" layer="21"/>
<wire x1="84.6" y1="54" x2="70" y2="54" width="0.127" layer="21"/>
<wire x1="70" y1="54" x2="70" y2="41" width="0.127" layer="21"/>
<wire x1="70" y1="41" x2="84.8" y2="41" width="0.127" layer="21"/>
<text x="5.3" y="0.2" size="0.8128" layer="21">J1</text>
<text x="37.8" y="11.3" size="0.8128" layer="21">J6</text>
<text x="43.7" y="20.7" size="0.8128" layer="21" rot="R90">J3</text>
<text x="58.5" y="1.2" size="0.8128" layer="21" rot="R180">J7</text>
<text x="84.5" y="19.4" size="0.8128" layer="21" rot="R180">J10</text>
<text x="84.6" y="36.1" size="0.8128" layer="21" rot="R180">J11</text>
<text x="83.8" y="55" size="0.8128" layer="21" rot="R180">J12</text>
<text x="14" y="48" size="1.016" layer="21">GPIO</text>
<text x="9" y="25" size="1.016" layer="21" rot="R90">DISPLAY</text>
<text x="16.2" y="0.1" size="1.016" layer="21" rot="R90">PWR IN</text>
<text x="41.3" y="0.4" size="1.016" layer="21" rot="R90">HDMI</text>
<text x="43.4" y="7.9" size="1.016" layer="21" rot="R90">CAMERA</text>
<text x="49.6" y="0.4" size="1.016" layer="21" rot="R90">A/V</text>
<text x="83" y="2" size="1.016" layer="21" rot="R180">ETHERNET</text>
<text x="84.5" y="21.7" size="1.016" layer="21" rot="R180">USB</text>
<text x="81.6" y="39.6" size="1.016" layer="21">USB</text>
<text x="6" y="37" size="0.8128" layer="21">J4</text>
<text x="23.21" y="43.18" size="1.778" layer="21">Raspberry Pi 3 Model B V1.2</text>
<rectangle x1="0.6" y1="42.5" x2="2.6" y2="47.75" layer="51"/>
<wire x1="0.6" y1="42.5" x2="2.6" y2="42.5" width="0.127" layer="21"/>
<wire x1="2.6" y1="42.5" x2="2.6" y2="47.75" width="0.127" layer="21"/>
<wire x1="2.6" y1="47.75" x2="0.6" y2="47.75" width="0.127" layer="21"/>
<wire x1="0.6" y1="47.75" x2="0.6" y2="42.5" width="0.127" layer="21"/>
<text x="4" y="42" size="1.016" layer="21" rot="R90">ANTENNA</text>
</package>
</packages>
<symbols>
<symbol name="RPI3">
<description>Symbol for the Raspberry Pi 3 Single Board Computer created by Dave Young for www.element14.com
For more documentation, visit http://www.raspberrypi.org/documentation/hardware/raspberrypi/</description>
<pin name="3V3@1" x="10.16" y="71.12" length="short" rot="R90"/>
<pin name="5V@2" x="10.16" y="99.06" length="short" rot="R270"/>
<pin name="GPIO2" x="15.24" y="71.12" length="short" rot="R90"/>
<pin name="5V@4" x="15.24" y="99.06" length="short" rot="R270"/>
<pin name="GPIO3" x="20.32" y="71.12" length="short" rot="R90"/>
<pin name="GND@6" x="20.32" y="99.06" length="short" rot="R270"/>
<pin name="GPIO4" x="25.4" y="71.12" length="short" rot="R90"/>
<pin name="GPIO14" x="25.4" y="99.06" length="short" rot="R270"/>
<pin name="GND@9" x="30.48" y="71.12" length="short" rot="R90"/>
<pin name="GPIO15" x="30.48" y="99.06" length="short" rot="R270"/>
<pin name="GPIO17" x="35.56" y="71.12" length="short" rot="R90"/>
<pin name="GPIO18" x="35.56" y="99.06" length="short" rot="R270"/>
<pin name="GPIO27" x="40.64" y="71.12" length="short" rot="R90"/>
<pin name="GND@14" x="40.64" y="99.06" length="short" rot="R270"/>
<pin name="GPIO22" x="45.72" y="71.12" length="short" rot="R90"/>
<pin name="GPIO23" x="45.72" y="99.06" length="short" rot="R270"/>
<pin name="3V3@17" x="50.8" y="71.12" length="short" rot="R90"/>
<pin name="GPIO24" x="50.8" y="99.06" length="short" rot="R270"/>
<pin name="GPIO10" x="55.88" y="71.12" length="short" rot="R90"/>
<pin name="GND@20" x="55.88" y="99.06" length="short" rot="R270"/>
<pin name="GPIO9" x="60.96" y="71.12" length="short" rot="R90"/>
<pin name="GPIO25" x="60.96" y="99.06" length="short" rot="R270"/>
<pin name="GPIO11" x="66.04" y="71.12" length="short" rot="R90"/>
<pin name="GPIO8" x="66.04" y="99.06" length="short" rot="R270"/>
<pin name="GND@25" x="71.12" y="71.12" length="short" rot="R90"/>
<pin name="GPIO7" x="71.12" y="99.06" length="short" rot="R270"/>
<pin name="ID_SD*" x="76.2" y="71.12" length="short" rot="R90"/>
<pin name="ID_SC*" x="76.2" y="99.06" length="short" rot="R270"/>
<pin name="GPIO5" x="81.28" y="71.12" length="short" rot="R90"/>
<pin name="GND@30" x="81.28" y="99.06" length="short" rot="R270"/>
<pin name="GPIO6" x="86.36" y="71.12" length="short" rot="R90"/>
<pin name="GPIO12" x="86.36" y="99.06" length="short" rot="R270"/>
<pin name="GPIO13" x="91.44" y="71.12" length="short" rot="R90"/>
<pin name="GND@34" x="91.44" y="99.06" length="short" rot="R270"/>
<pin name="GPIO19" x="96.52" y="71.12" length="short" rot="R90"/>
<pin name="GPIO16" x="96.52" y="99.06" length="short" rot="R270"/>
<pin name="GPIO26" x="101.6" y="71.12" length="short" rot="R90"/>
<pin name="GPIO20" x="101.6" y="99.06" length="short" rot="R270"/>
<pin name="GND@39" x="106.68" y="71.12" length="short" rot="R90"/>
<pin name="GPIO21" x="106.68" y="99.06" length="short" rot="R270"/>
<text x="91.44" y="66.04" size="1.778" layer="95">*ID_SD and ID_SC PINS:</text>
<text x="91.44" y="54.61" size="1.016" layer="95">These pins are reserved for ID EEPROM

At boot time this I2C interface will be
interrogated to look for an EEPROM
that identifies the attached board and
allows automagic setup of the GPIOs
(and optionally, Linux drivers).</text>
<text x="91.44" y="48.26" size="1.016" layer="95" ratio="24" distance="60">DO NOT USE these pins for anything other
than attaching an I2C ID EEPROM. Leave
unconnected if ID EEPROM not required.</text>
<wire x1="7.62" y1="96.52" x2="109.22" y2="96.52" width="0.254" layer="94" style="shortdash"/>
<wire x1="109.22" y1="96.52" x2="109.22" y2="73.66" width="0.254" layer="94" style="shortdash"/>
<wire x1="109.22" y1="73.66" x2="7.62" y2="73.66" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="73.66" x2="7.62" y2="96.52" width="0.254" layer="94" style="shortdash"/>
<text x="2.54" y="76.2" size="1.778" layer="95" rot="R90">J8</text>
<text x="5.08" y="76.2" size="1.778" layer="95" rot="R90">GPIO Expansion</text>
<polygon width="0.254" layer="94">
<vertex x="5.08" y="50.8"/>
<vertex x="7.62" y="50.8"/>
<vertex x="7.62" y="17.78"/>
<vertex x="5.08" y="17.78"/>
<vertex x="5.08" y="22.86"/>
<vertex x="2.54" y="25.4"/>
<vertex x="2.54" y="43.18"/>
<vertex x="5.08" y="45.72"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="81.28" y="5.08"/>
<vertex x="78.74" y="5.08"/>
<vertex x="78.74" y="38.1"/>
<vertex x="81.28" y="38.1"/>
<vertex x="81.28" y="33.02"/>
<vertex x="83.82" y="30.48"/>
<vertex x="83.82" y="12.7"/>
<vertex x="81.28" y="10.16"/>
</polygon>
<rectangle x1="15.24" y1="-2.54" x2="30.48" y2="10.16" layer="94"/>
<rectangle x1="43.18" y1="-2.54" x2="71.12" y2="12.7" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="91.44" y="0"/>
<vertex x="91.44" y="-5.08"/>
<vertex x="99.06" y="-5.08"/>
<vertex x="99.06" y="0"/>
<vertex x="101.6" y="0"/>
<vertex x="101.6" y="20.32"/>
<vertex x="88.9" y="20.32"/>
<vertex x="88.9" y="0"/>
</polygon>
<rectangle x1="124.46" y1="2.54" x2="162.56" y2="33.02" layer="94"/>
<rectangle x1="129.54" y1="45.72" x2="162.56" y2="68.58" layer="94"/>
<rectangle x1="129.54" y1="73.66" x2="162.56" y2="96.52" layer="94"/>
<rectangle x1="12.7" y1="27.94" x2="35.56" y2="43.18" layer="94"/>
<wire x1="0" y1="0" x2="160.02" y2="0" width="0.254" layer="94"/>
<wire x1="160.02" y1="0" x2="160.02" y2="101.6" width="0.254" layer="94"/>
<wire x1="160.02" y1="101.6" x2="0" y2="101.6" width="0.254" layer="94"/>
<wire x1="0" y1="101.6" x2="0" y2="0" width="0.254" layer="94"/>
<text x="25.4" y="53.34" size="5.08" layer="94">Raspberry Pi 3</text>
<text x="10.16" y="25.4" size="1.778" layer="94" rot="R90">J4, DISPLAY</text>
<text x="12.7" y="25.908" size="1.778" layer="94">MICRO SD (bottom)</text>
<text x="30.226" y="12.192" size="1.778" layer="94" rot="R180">MICRO USB</text>
<text x="51.054" y="13.462" size="1.778" layer="94">J6, HDMI</text>
<text x="77.978" y="13.716" size="1.778" layer="94" rot="R90">J3, CAMERA</text>
<text x="100.076" y="22.606" size="1.778" layer="94" rot="R180">J7, A/V</text>
<text x="121.92" y="27.94" size="1.778" layer="94" rot="R270">J10, ETHERNET</text>
<text x="127" y="63.5" size="1.778" layer="94" rot="R270">J11, 2X USB</text>
<text x="127" y="91.44" size="1.778" layer="94" rot="R270">J12, 2X USB</text>
<text x="13.462" y="43.688" size="1.778" layer="94">J9</text>
<text x="20.32" y="12.7" size="1.778" layer="94">J1</text>
<rectangle x1="2.54" y1="58.42" x2="5.08" y2="66.04" layer="94"/>
<text x="7.62" y="56.642" size="1.778" layer="94" rot="R90">ANTENNA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RPI3" prefix="PCB">
<description>Device for Raspberry Pi 3 board created by Dave Young for www.element14.com
More information and support community is available at https://www.element14.com/community/community/raspberry-pi</description>
<gates>
<gate name="G$1" symbol="RPI3" x="-81.28" y="-50.8"/>
</gates>
<devices>
<device name="" package="RPI3">
<connects>
<connect gate="G$1" pin="3V3@1" pad="P$1"/>
<connect gate="G$1" pin="3V3@17" pad="P$17"/>
<connect gate="G$1" pin="5V@2" pad="P$2"/>
<connect gate="G$1" pin="5V@4" pad="P$4"/>
<connect gate="G$1" pin="GND@14" pad="P$14"/>
<connect gate="G$1" pin="GND@20" pad="P$20"/>
<connect gate="G$1" pin="GND@25" pad="P$25"/>
<connect gate="G$1" pin="GND@30" pad="P$30"/>
<connect gate="G$1" pin="GND@34" pad="P$34"/>
<connect gate="G$1" pin="GND@39" pad="P$39"/>
<connect gate="G$1" pin="GND@6" pad="P$6"/>
<connect gate="G$1" pin="GND@9" pad="P$9"/>
<connect gate="G$1" pin="GPIO10" pad="P$19"/>
<connect gate="G$1" pin="GPIO11" pad="P$23"/>
<connect gate="G$1" pin="GPIO12" pad="P$32"/>
<connect gate="G$1" pin="GPIO13" pad="P$33"/>
<connect gate="G$1" pin="GPIO14" pad="P$8"/>
<connect gate="G$1" pin="GPIO15" pad="P$10"/>
<connect gate="G$1" pin="GPIO16" pad="P$36"/>
<connect gate="G$1" pin="GPIO17" pad="P$11"/>
<connect gate="G$1" pin="GPIO18" pad="P$12"/>
<connect gate="G$1" pin="GPIO19" pad="P$35"/>
<connect gate="G$1" pin="GPIO2" pad="P$3"/>
<connect gate="G$1" pin="GPIO20" pad="P$38"/>
<connect gate="G$1" pin="GPIO21" pad="P$40"/>
<connect gate="G$1" pin="GPIO22" pad="P$15"/>
<connect gate="G$1" pin="GPIO23" pad="P$16"/>
<connect gate="G$1" pin="GPIO24" pad="P$18"/>
<connect gate="G$1" pin="GPIO25" pad="P$22"/>
<connect gate="G$1" pin="GPIO26" pad="P$37"/>
<connect gate="G$1" pin="GPIO27" pad="P$13"/>
<connect gate="G$1" pin="GPIO3" pad="P$5"/>
<connect gate="G$1" pin="GPIO4" pad="P$7"/>
<connect gate="G$1" pin="GPIO5" pad="P$29"/>
<connect gate="G$1" pin="GPIO6" pad="P$31"/>
<connect gate="G$1" pin="GPIO7" pad="P$26"/>
<connect gate="G$1" pin="GPIO8" pad="P$24"/>
<connect gate="G$1" pin="GPIO9" pad="P$21"/>
<connect gate="G$1" pin="ID_SC*" pad="P$28"/>
<connect gate="G$1" pin="ID_SD*" pad="P$27"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="st-microelectronics" urn="urn:adsk.eagle:library:368">
<description>&lt;b&gt;ST Microelectronics Devices&lt;/b&gt;&lt;p&gt;
Microcontrollers,  I2C components, linear devices&lt;p&gt;
http://www.st.com&lt;p&gt;
&lt;i&gt;Include st-microelectronics-2.lbr, st-microelectronics-3.lbr.&lt;p&gt;&lt;/i&gt;

&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16" urn="urn:adsk.eagle:footprint:26704/1" library_version="4">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DIL16" urn="urn:adsk.eagle:package:26820/1" type="box" library_version="4">
<description>Dual In Line Package</description>
<packageinstances>
<packageinstance name="DIL16"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="L293D" urn="urn:adsk.eagle:symbol:26701/1" library_version="4">
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="21.336" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1-2EN" x="-15.24" y="17.78" length="middle" direction="in"/>
<pin name="1A" x="-15.24" y="12.7" length="middle" direction="in"/>
<pin name="1Y" x="-15.24" y="7.62" length="middle" direction="out"/>
<pin name="GND1" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="GND2" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="2Y" x="-15.24" y="-7.62" length="middle" direction="out"/>
<pin name="2A" x="-15.24" y="-12.7" length="middle" direction="in"/>
<pin name="VCC2" x="-15.24" y="-17.78" length="middle" direction="pwr"/>
<pin name="VCC1" x="15.24" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="4A" x="15.24" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="4Y" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="GND3" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND4" x="15.24" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="3Y" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="3A" x="15.24" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="3-4EN" x="15.24" y="-17.78" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="L293D" urn="urn:adsk.eagle:component:26880/1" prefix="IC" library_version="4">
<description>&lt;b&gt;PUSH-PULL 4 CHANNEL DRIVER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="L293D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL16">
<connects>
<connect gate="G$1" pin="1-2EN" pad="1"/>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1Y" pad="3"/>
<connect gate="G$1" pin="2A" pad="7"/>
<connect gate="G$1" pin="2Y" pad="6"/>
<connect gate="G$1" pin="3-4EN" pad="9"/>
<connect gate="G$1" pin="3A" pad="10"/>
<connect gate="G$1" pin="3Y" pad="11"/>
<connect gate="G$1" pin="4A" pad="15"/>
<connect gate="G$1" pin="4Y" pad="14"/>
<connect gate="G$1" pin="GND1" pad="4"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="GND3" pad="13"/>
<connect gate="G$1" pin="GND4" pad="12"/>
<connect gate="G$1" pin="VCC1" pad="16"/>
<connect gate="G$1" pin="VCC2" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26820/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="L293D" constant="no"/>
<attribute name="OC_FARNELL" value="9589619" constant="no"/>
<attribute name="OC_NEWARK" value="56P8249" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="battery" urn="urn:adsk.eagle:library:109">
<description>&lt;b&gt;Lithium Batteries and NC Accus&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="AB9V" urn="urn:adsk.eagle:footprint:4514/1" library_version="2">
<description>&lt;B&gt;9-V BATTERY CLIP&lt;/B&gt;</description>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-3.175" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="4.318" x2="-1.778" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="5.08" x2="1.778" y2="5.08" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.318" x2="1.778" y2="5.08" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.318" x2="2.54" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-5.08" x2="2.54" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-5.08" x2="-1.778" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-1.778" y2="-5.08" width="0.1524" layer="21"/>
<circle x="0" y="2.54" radius="1.27" width="0.1524" layer="51"/>
<circle x="0" y="-2.54" radius="1.27" width="0.1524" layer="51"/>
<pad name="-" x="0" y="-2.54" drill="1.1176" diameter="3.1496" shape="octagon"/>
<pad name="+" x="0" y="2.54" drill="1.1176" diameter="3.1496" shape="octagon"/>
<text x="-1.905" y="-1.143" size="1.27" layer="21" ratio="16" rot="R90">9V</text>
<text x="-3.81" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="AB9V" urn="urn:adsk.eagle:package:4600/1" type="box" library_version="2">
<description>9-V BATTERY CLIP</description>
<packageinstances>
<packageinstance name="AB9V"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="9V0-BL" urn="urn:adsk.eagle:symbol:4513/1" library_version="2">
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="0" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="0" x2="-6.985" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="0" x2="-6.985" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="0.635" x2="-4.445" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="6.985" y1="2.54" x2="6.985" y2="0" width="0.4064" layer="94"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.1524" layer="94"/>
<wire x1="6.985" y1="0" x2="6.985" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="2.54" x2="-3.175" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0.635" x2="3.175" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="4.445" y1="2.54" x2="4.445" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="5.715" y1="0.635" x2="5.715" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="9.525" y1="0.635" x2="9.525" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-9.525" y1="0.635" x2="-9.525" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="8.255" y1="0" x2="8.89" y2="0" width="0.6096" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.795" y2="0" width="0.6096" layer="94"/>
<text x="-5.715" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="7.62" y="0.635" size="1.524" layer="94">+</text>
<text x="-8.255" y="0.635" size="1.524" layer="94">-</text>
<pin name="+" x="12.7" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="-" x="-12.7" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AB9V" urn="urn:adsk.eagle:component:4654/1" prefix="G" library_version="2">
<description>&lt;B&gt;9-V BATTERY CLIP&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="9V0-BL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AB9V">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:4600/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Electromechanical" urn="urn:adsk.eagle:library:516">
<description>&lt;h3&gt;SparkFun Electromechanical Parts&lt;/h3&gt;
This library contains electromechanical devices, like motors, speakers,servos, and relays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="VIBE-MOTOR-10MM" urn="urn:adsk.eagle:footprint:38542/1" library_version="1">
<description>&lt;h3&gt;Vibration Motor&lt;/h3&gt;
&lt;p&gt;10mm vibration motor terminated with tinned wires.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Robotics/B1034.FL45-00-015.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;Devices Using&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;MOTOR&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-1.5" y1="-4.8" x2="-1.5" y2="-7.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-7.8" x2="1.5" y2="-7.8" width="0.127" layer="51"/>
<wire x1="1.5" y1="-7.8" x2="1.5" y2="-4.8" width="0.127" layer="51"/>
<circle x="0" y="0" radius="5.0009" width="0.127" layer="51"/>
<pad name="+" x="-5" y="-3.9" drill="0.8" diameter="1.6764"/>
<pad name="-" x="-6.35" y="-2.54" drill="0.8" diameter="1.6764"/>
<text x="-7.562" y="-2" size="1.27" layer="21">-</text>
<text x="-4.446" y="-5.554" size="1.27" layer="21">+</text>
<text x="-5.207" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="-4.699" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="VIBE-MOTOR-10MM" urn="urn:adsk.eagle:package:38555/1" type="box" library_version="1">
<description>Vibration Motor
10mm vibration motor terminated with tinned wires.
Datasheet
Devices Using
MOTOR</description>
<packageinstances>
<packageinstance name="VIBE-MOTOR-10MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MOTOR" urn="urn:adsk.eagle:symbol:38541/1" library_version="1">
<description>&lt;h3&gt;Motor (Single-Phase, Two-Terminal)&lt;/h3&gt;</description>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="1.778" width="0.2032" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.2032" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="3.81" width="0.2032" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="1.778" width="0.2032" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="1.27" y2="-1.778" width="0.2032" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="0" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="0" y1="-3.81" x2="-1.27" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-1.778" width="0.2032" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0.254" y1="3.302" x2="-0.254" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="3.556" x2="0" y2="3.048" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-3.302" x2="-0.254" y2="-3.302" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="2.032" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="+" x="0" y="5.08" visible="off" length="point" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="point" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOTOR" urn="urn:adsk.eagle:component:38564/1" prefix="M" library_version="1">
<description>&lt;h3&gt;Motor (Single-Phase)&lt;/h3&gt;
&lt;p&gt;Two-terminal, single-phase motors.&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8449"&gt;Vibration Motor&lt;/a&gt; (ROB-08449)
&lt;br&gt;Physical dimension and wire connections for this powerful vibration motor. Motor has a self adhesive backing.&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="MOTOR" x="0" y="0"/>
</gates>
<devices>
<device name="10MM" package="VIBE-MOTOR-10MM">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38555/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="COMP-08572"/>
<attribute name="SF_SKU" value="ROB-08449"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="XL6009-TO263">
<description>&lt;b&gt;Boost Convertor IC&lt;/b&gt;
&lt;p&gt;Created by Manash Jyoti Deka &lt;a href="https://github.com/mjdeka"&gt;&lt;/a&gt;&lt;/b&gt;</description>
<packages>
<package name="TO263-5">
<description>&lt;b&gt;TO-263&lt;/b&gt;</description>
<wire x1="5.094" y1="7.165" x2="5.094" y2="-2.88" width="0.2032" layer="51"/>
<wire x1="5.094" y1="-2.88" x2="-5.094" y2="-2.88" width="0.2032" layer="21"/>
<wire x1="-5.094" y1="-2.88" x2="-5.094" y2="7.165" width="0.2032" layer="51"/>
<wire x1="-5.094" y1="7.165" x2="5.094" y2="7.165" width="0.2032" layer="51"/>
<wire x1="-5.105" y1="7.267" x2="-5.105" y2="7.678" width="0.2032" layer="51"/>
<wire x1="-5.105" y1="7.678" x2="-3.378" y2="8.135" width="0.2032" layer="51"/>
<wire x1="-3.378" y1="8.135" x2="3.378" y2="8.135" width="0.2032" layer="51"/>
<wire x1="3.378" y1="8.135" x2="5.105" y2="7.678" width="0.2032" layer="51"/>
<wire x1="5.105" y1="7.678" x2="5.105" y2="7.267" width="0.2032" layer="51"/>
<wire x1="5.105" y1="7.267" x2="-5.105" y2="7.267" width="0.2032" layer="51"/>
<smd name="6" x="0" y="2.54" dx="10.8" dy="10.41" layer="1"/>
<smd name="1" x="-3.4036" y="-5.7912" dx="1.0668" dy="2.159" layer="1"/>
<smd name="2" x="-1.7018" y="-5.7912" dx="1.0668" dy="2.159" layer="1"/>
<smd name="3" x="0" y="-5.7912" dx="1.0668" dy="2.159" layer="1"/>
<smd name="4" x="1.7018" y="-5.7912" dx="1.0668" dy="2.159" layer="1"/>
<smd name="5" x="3.4036" y="-5.7912" dx="1.0668" dy="2.159" layer="1"/>
<text x="-5.588" y="-5.588" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.858" y="-5.588" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.8862" y1="-6.604" x2="-2.921" y2="-4.445" layer="51"/>
<rectangle x1="-3.8862" y1="-4.4704" x2="-2.921" y2="-2.794" layer="21"/>
<rectangle x1="-2.1844" y1="-6.604" x2="-1.2192" y2="-4.445" layer="51"/>
<rectangle x1="-0.4826" y1="-6.604" x2="0.4826" y2="-4.445" layer="51"/>
<rectangle x1="1.2192" y1="-6.604" x2="2.1844" y2="-4.445" layer="51"/>
<rectangle x1="2.921" y1="-6.604" x2="3.8862" y2="-4.445" layer="51"/>
<rectangle x1="-2.1844" y1="-4.4704" x2="-1.2192" y2="-2.794" layer="21"/>
<rectangle x1="-0.4826" y1="-4.4704" x2="0.4826" y2="-2.794" layer="21"/>
<rectangle x1="1.2192" y1="-4.4704" x2="2.1844" y2="-2.794" layer="21"/>
<rectangle x1="2.921" y1="-4.4704" x2="3.8862" y2="-2.794" layer="21"/>
<polygon width="0.2032" layer="51">
<vertex x="-5.1054" y="7.267"/>
<vertex x="-5.1054" y="7.6782"/>
<vertex x="-3.3782" y="8.1354"/>
<vertex x="3.3782" y="8.1354"/>
<vertex x="5.1054" y="7.6782"/>
<vertex x="5.1054" y="7.267"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="XL6009">
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="10.82" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="-7.62" length="short" direction="pwr"/>
<pin name="EN" x="-10.16" y="2.54" length="short" direction="in"/>
<pin name="SW" x="-10.16" y="-2.54" length="short" direction="pwr"/>
<pin name="FB" x="-10.16" y="-12.7" length="short" direction="out"/>
<pin name="GND" x="-10.16" y="7.62" length="short" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="XL6009" prefix="IC">
<description>XL6009.
400KHz 60V 4A Switching Current Boost / Buck-Boost / Inverting DC/DC Converter.

Created By: Manash Jyoti Deka. Github: https://github.com/mjdeka</description>
<gates>
<gate name="A" symbol="XL6009" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="TO263-5">
<connects>
<connect gate="A" pin="EN" pad="2"/>
<connect gate="A" pin="FB" pad="5"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="SW" pad="3"/>
<connect gate="A" pin="VIN" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="PCB1" library="e14_Rpi3_RevA" deviceset="RPI3" device=""/>
<part name="IC1" library="st-microelectronics" library_urn="urn:adsk.eagle:library:368" deviceset="L293D" device="" package3d_urn="urn:adsk.eagle:package:26820/1"/>
<part name="BATTERY" library="battery" library_urn="urn:adsk.eagle:library:109" deviceset="AB9V" device="" package3d_urn="urn:adsk.eagle:package:4600/1" value="12V"/>
<part name="MOTOR" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="MOTOR" device="10MM" package3d_urn="urn:adsk.eagle:package:38555/1" value=""/>
<part name="BOOSTCONVERTER" library="XL6009-TO263" deviceset="XL6009" device="S"/>
</parts>
<sheets>
<sheet>
<description>Everything</description>
<plain>
</plain>
<instances>
<instance part="PCB1" gate="G$1" x="-83.82" y="2.54" smashed="yes"/>
<instance part="IC1" gate="G$1" x="-165.1" y="81.28" smashed="yes">
<attribute name="NAME" x="-175.26" y="102.616" size="1.778" layer="95"/>
<attribute name="VALUE" x="-175.26" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="BATTERY" gate="G$1" x="-132.08" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="-128.905" y="74.295" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-137.16" y="74.295" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="MOTOR" gate="G$1" x="-177.8" y="22.86" smashed="yes">
<attribute name="NAME" x="-175.26" y="22.86" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-175.26" y="20.32" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="BOOSTCONVERTER" gate="A" x="-132.08" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="-121.26" y="38.1" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-149.86" y="38.1" size="1.778" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="-177.8" y1="27.94" x2="-182.88" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="27.94" x2="-182.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="1Y"/>
<wire x1="-182.88" y1="88.9" x2="-180.34" y2="88.9" width="0.1524" layer="91"/>
<pinref part="MOTOR" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND4"/>
<pinref part="IC1" gate="G$1" pin="GND3"/>
<wire x1="-149.86" y1="78.74" x2="-149.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="BOOSTCONVERTER" gate="A" pin="FB"/>
<wire x1="-149.86" y1="81.28" x2="-149.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="48.26" x2="-144.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="81.28" x2="-149.86" y2="81.28" width="0.1524" layer="91"/>
<junction x="-149.86" y="81.28"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND1"/>
<pinref part="IC1" gate="G$1" pin="GND2"/>
<wire x1="-180.34" y1="83.82" x2="-180.34" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="2Y"/>
<wire x1="-180.34" y1="73.66" x2="-187.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="73.66" x2="-187.96" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="17.78" x2="-177.8" y2="17.78" width="0.1524" layer="91"/>
<pinref part="MOTOR" gate="G$1" pin="-"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="1A"/>
<wire x1="-180.34" y1="93.98" x2="-185.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="93.98" x2="-185.42" y2="116.84" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="GPIO23"/>
<wire x1="-185.42" y1="116.84" x2="-38.1" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="116.84" x2="-38.1" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="2A"/>
<wire x1="-180.34" y1="68.58" x2="-190.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="68.58" x2="-190.5" y2="124.46" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="GPIO24"/>
<wire x1="-190.5" y1="124.46" x2="-33.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="124.46" x2="-33.02" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCC1"/>
<wire x1="-149.86" y1="99.06" x2="-93.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="99.06" x2="-93.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="5V@2"/>
<wire x1="-93.98" y1="109.22" x2="-73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="109.22" x2="-73.66" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="1-2EN"/>
<wire x1="-180.34" y1="99.06" x2="-180.34" y2="114.3" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="GPIO18"/>
<wire x1="-180.34" y1="114.3" x2="-48.26" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="114.3" x2="-48.26" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="BATTERY" gate="G$1" pin="-"/>
<wire x1="-132.08" y1="81.28" x2="-124.46" y2="81.28" width="0.1524" layer="91"/>
<pinref part="BOOSTCONVERTER" gate="A" pin="GND"/>
<wire x1="-124.46" y1="81.28" x2="-124.46" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="BATTERY" gate="G$1" pin="+"/>
<wire x1="-132.08" y1="55.88" x2="-139.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="55.88" x2="-139.7" y2="48.26" width="0.1524" layer="91"/>
<pinref part="BOOSTCONVERTER" gate="A" pin="VIN"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="BOOSTCONVERTER" gate="A" pin="SW"/>
<wire x1="-134.62" y1="48.26" x2="-134.62" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="53.34" x2="-180.34" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VCC2"/>
<wire x1="-180.34" y1="63.5" x2="-180.34" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>sensor</description>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
