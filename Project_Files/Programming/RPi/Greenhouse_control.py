import Motor_lib, i2c_lib, sys

Motor_lib.setup()

temp = 0
humid = 0

roofState = 0
fanState = 0

openTime = 2
closeTime = 1.5


def isOpen():
    return roofState == 1

def isClosed():
    return roofState == 0

while True:
    try:
        temp = i2c_lib.getTemp()
        humid = i2c_lib.getHumid()
        print("Temp:{:d}\tHumid:{:d}\t{:d}".format(temp, humid,roofState))


        #Control roof based on temprature
        if ((temp > 27) and (isClosed())):
            Motor_lib.open(openTime)
            roofState = 1
        elif ((temp < 25) and (isOpen())):
            Motor_lib.close(closeTime)
            roofState = 0

        #Control fan based on humidity
        if (humid > 80):
            Motor_lib.FanOn(fanState)
            fanState = 1
        elif (humid < 65):
            Motor_lib.FanOff(fanState)
            fanState = 0

        Motor_lib.sleep(1)
    except:
        Motor_lib.stop()
        print ("Unexpected error:", sys.exc_info()[0])
        sys.exit(0)


    
