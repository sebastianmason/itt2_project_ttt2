import smbus
import time

DEV_ADDR = 0x04

bus = smbus.SMBus(1)
reads = 0
errs = 0

numbers=[0,0]

h, t = 0, 0


while True:
    reads += 1
    try:
        numbers[1] = bus.read_byte(DEV_ADDR)
        numbers[0] = bus.read_byte(DEV_ADDR)
        t = int(''.join('{:02X}'.format(numbers[0])),16)
        h = int(''.join('{:02X}'.format(numbers[1])),16)
        print("Humid: %s\t\tTemp: %s; no. of reads [%s]; no. of errors [%s]" % (h, t, reads, errs))
    except Exception as ex:
        errs += 1
        print("Exception [%s]" % (ex))

    time.sleep(2)

