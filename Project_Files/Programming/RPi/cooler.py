import RPi.GPIO as GPIO
import i2c_lib
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(13, GPIO.OUT)
GPIO.output(13,True)

def trigger(trigval,humidval):
    try:
        if humidval<trigval:
           # print("Not Spinning")
            GPIO.output(13,True)
        else:
           # print("Spinning")
            GPIO.output(13,False)
    except KeyboardInterrupt:
        print("Exiting")
        sys.exit(0)

if __name__=="__main__":
    try:
        while True:
            trigger(75,i2c_lib.getHumid())
    except KeyboardInterrupt:
        GPIO.output(13, True)
        sys.exit(0)
