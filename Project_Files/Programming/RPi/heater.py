import RPi.GPIO as GPIO
import i2c_lib
import sys
import Adafruit_MotorTest as motor
import time

step_delay = 10 / 1000.0

def trigger(trigval,tempval):
    try:
        if tempval<trigval:
            motor.backwards(step_delay, 200)
        else:
            motor.forward(step_delay, 200)
    except KeyboardInterrupt:
        print("Exiting")
        sys.exit(0)

if __name__=="__main__":
    try:
        while True:
            t = i2c_lib.getTemp()
            trigger(27, t)
            print(t)
            time.sleep(10)
    except KeyboardInterrupt:
        sys.exit(0)
