import smbus

DEV_ADDR = 0x04

bus = smbus.SMBus(1)

numbers=[0,0]

def getTemp():
    try:
        __getVals()
        t = int(''.join('{:02X}'.format(numbers[0])),16)
        return t
    except Exception as ex:
        print("Exception [%s]" % (ex))

def getHumid():
    try:
        __getVals()
        h = int(''.join('{:02X}'.format(numbers[1])),16)
        return h
    except Exception as ex:
        print("Exception [%s]" % (ex))

def __getVals():
    try:
        numbers[1] = bus.read_byte(DEV_ADDR)
        numbers[0] = bus.read_byte(DEV_ADDR)
    except Exception as ex:
        print("Exception [%s]" % (ex))