from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
import i2c_lib
import Motor_lib

app = Flask(__name__)
CORS(app, resources=r'/*')  # to allow external resources to fetch data
api = Api(app)


roofState = 0
fanState = 0

@api.resource("/")
class url_index(Resource):
    def get(self):
        returnMessage = {"message": "Yes, it works"}
        return returnMessage


@api.resource("/temp")
class url_temp(Resource):
    def get(self):
        returnMessage = i2c_lib.getTemp()
        return returnMessage


@api.resource("/humid")
class url_humid(Resource):
    def get(self):
        returnMessage = i2c_lib.getHumid()
        return returnMessage


@api.resource("/fan")
class url_fan(Resource):
    def get(self):
        returnMessage = fanState
        return returnMessage

    def put(self):
        global fanState
        if fanState:
            returnMessage = Motor_lib.FanOff(fanState)
        else:
            returnMessage = Motor_lib.FanOn(fanState)
        fanState = not fanState
        return returnMessage


@api.resource("/roof")
class url_roof(Resource):
    def get(self):
        returnMessage = roofState
        return returnMessage

    def post(self):
        roofState = request.json["data"]
        if roofState:
            returnMessage = Motor_lib.close(1)
        else:
            returnMessage = Motor_lib.open(1)
        roofState = not roofState
        return returnMessage

if __name__ == '__main__':
    Motor_lib.setup()
    app.run(host="0.0.0.0", debug=True)

