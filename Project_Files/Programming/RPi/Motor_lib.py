import RPi.GPIO as GPIO
from time import sleep

Motor1A = 22    # Motor Input Pin A
Motor1B = 18    # Motor Input Pin B
Motor1En = 12   # Motor Enable Pin

Fanpin = 13     # Fan Enable Pin



def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)

    GPIO.setup(Motor1A,GPIO.OUT)
    GPIO.setup(Motor1B,GPIO.OUT)
    GPIO.setup(Motor1En,GPIO.OUT)
    GPIO.setup(Fanpin,GPIO.OUT)


def FanOn(currentState):
    if not currentState:
        print("Turning on fan")
    GPIO.output(Fanpin, GPIO.LOW)    


def FanOff(currentState):
    if currentState:
        print("Turning off fan")
    GPIO.output(Fanpin, GPIO.HIGH)


def close(time):
    print ("Closing")
    GPIO.output(Motor1A,GPIO.HIGH)
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor1En,GPIO.HIGH)

    sleep(time)

    GPIO.output(Motor1En,GPIO.LOW)


def open(time):
    print ("Opening")
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.HIGH)
    GPIO.output(Motor1En,GPIO.HIGH)

    sleep(time)

    GPIO.output(Motor1En,GPIO.LOW)

def stop():
    GPIO.output(Motor1En,GPIO.LOW)
    GPIO.output(Fanpin, GPIO.HIGH)


if (__name__ == "__main__"):
    try:
        setup()
        open(2)
        close(1.5)
        GPIO.cleanup()
    except KeyboardInterrupt:
        stop()
