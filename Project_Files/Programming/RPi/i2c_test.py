import smbus
import time

DEV_ADDR = 0x04

bus = smbus.SMBus(1)
reads = 0
errs = 0

numbers=[0,0]

int(''.join('{:02X}'.format(a) for a in numbers),16)



while True:
    reads += 1
    try:
        numbers[1] = bus.read_byte(DEV_ADDR)
        numbers[0] = bus.read_byte(DEV_ADDR)
        a_val = int(''.join('{:02X}'.format(a) for a in numbers),16)
        print("Read value [%s]; no. of reads [%s]; no. of errors [%s]" % (a_val, reads, errs))
    except Exception as ex:
        errs += 1
        print("Exception [%s]" % (ex))

    time.sleep(2)
