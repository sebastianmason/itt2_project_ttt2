/*
DHT11Tester_ATtiny.ino

This sketch demonstrates the usage of the DHT11 library on an ATtiny85. For details on wiring and information on possible compiler errors, see the link below.
http://projectsfromtech.blogspot.com/2014/02/dht11-on-attiny85.html

*/


#include <dht11.h>
#include <SoftwareSerial.h>
#include <TinyWireS.h>

dht11 DHT11;

#define DHT11PIN 3
#define I2C_SLAVE_ADDRESS 0x04


SoftwareSerial mySerial(1, 4); // RX, TX

byte reg[] = {0x00, 0x00};
byte reg_idx = 0;

void onRequest()
{
  TinyWireS.send(reg[reg_idx]);
  reg_idx = (reg_idx == 1) ? 0 : 1;
}


void setup()
{
  TinyWireS.begin(I2C_SLAVE_ADDRESS);
  TinyWireS.onRequest(onRequest);
  mySerial.begin(9600);
  mySerial.println("DHT11 TEST PROGRAM ");
  mySerial.print("LIBRARY VERSION: ");
  mySerial.println(DHT11LIB_VERSION);
  mySerial.println();
}

void loop()
{

  int chk = DHT11.read(DHT11PIN);

  mySerial.print("\n");
  mySerial.print("Read sensor: ");
  switch (chk)
  {
    case DHTLIB_OK:
      mySerial.println("OK");
      break;
    case DHTLIB_ERROR_CHECKSUM:
      mySerial.println("Checksum error");
      break;
    case DHTLIB_ERROR_TIMEOUT1:
      mySerial.println("Time out error 1");
      break;
    case DHTLIB_ERROR_TIMEOUT2:
      mySerial.println("Time out error 2");
      break;
    case DHTLIB_ERROR_TIMEOUT3:
      mySerial.println("Time out error 3");
      break;
    case DHTLIB_ERROR_TIMEOUT4:
      mySerial.println("Time out error 4");
      break;
    default:
      mySerial.println("Unknown error");
      break;
  }
  float h = (float)DHT11.humidity;
  float t = (float)DHT11.temperature;
  
  mySerial.print("Humidity (%): ");
  mySerial.println(h, 2);

  mySerial.print("Temperature (°C): ");
  mySerial.println(t, 2);

  reg[0] = lowByte((int)h);
  reg[1] = lowByte((int)t);

  delay(2000);
  
  
  TinyWireS_stop_check();
}
//
// END OF FILE
//
